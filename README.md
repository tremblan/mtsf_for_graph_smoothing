This repository contains the code necessary to reproduce the results in:

> Hugo Jaquard, Michaël Fanuel, Pierre-Olivier Amblard, Rémi Bardenet, Simon Barthelmé, Nicolas Tremblay      
> Smoothing complex-valued signals on Graphs with Monte-Carlo     
> arXiv.org:2210.08014
	
The Pluto notebook comes with an internal TOML description of the environment used for the paper.

If you have any question, please contact Hugo Jaquard at: firstname DOT lastname AT grenoble-inp DOT fr.
