function detect_cycle(previous,last_reached_node,next,seen,i)
	if seen[last_reached_node] == i 
		u = last_reached_node
		while next[u] != previous
			seen[u] = 0
			u = next[u]
		end
		return true
	end		
	
	return false
end

# MTSF based estimator

function estimator(q_vector,n,C,g,neighbors_list,neighbors_weights)

	f = zeros(Complex,n)
	next = zeros(Int,n)
	state = zeros(Int,n)
	local_holonomy = ones(Complex,n)
	seen = zeros(Int,n) 
	degree = sum.(neighbors_weights)
	i = 1
	value = 0
	is_cycle = false
	
	for v in 1:n
		u = v
		is_unicycle = false
		last_visited_node = u
		total_holonomy = 0

		while state[u] != -1 && state != -2 
			seen[u] = i
			is_cycle = false
			deg = degree[u]
			if rand() <= q_vector[u]/(q_vector[u]+deg)
				state[u] = -1
				next[u] = u
				last_visited_node = u	

				value = g[u]
				f[u] = g[u]
			else
				next[u] = sample(neighbors_list[u],Weights(neighbors_weights[u]))
				local_holonomy[next[u]] = local_holonomy[u]/C[u,next[u]]
				is_cycle = detect_cycle(u,next[u],next,seen,i)
				u = next[u]

				value = f[u] 
			end
			
			if is_cycle
				if rand() <= 1-cos(angle(local_holonomy[u]/(local_holonomy[next[u]]*C[u,next[u]]))) 
					state[u] = -2 
				else 
					local_holonomy[u] = local_holonomy[next[u]]*C[u,next[u]]
				end
			end
		end

		i += 1

		# adding the new branch and propagating the value
		
		u = v
		while state[u] != -1 && (state[u] != -2 || state[next[u]] != -2)
			if state[last_visited_node] != -2
				f[u] = (local_holonomy[u])*value
				state[u] = -1
			else
				state[u] = -2
			end

			u = next[u]
		end

	end

	return f
end

# computes the empirical expectation of the estimator over nb_samples realizations

function estimate(q_vector,n,C,g,nb_samples,neighbors_list,neighbors_weights)
	x = zeros(n)
	for k in 1:nb_samples
		x += estimator(q_vector,n,C,g,neighbors_list,neighbors_weights)
	end
	return x/nb_samples
end

# Rao-Blackwell version of the estimator

function rao_estimator(q_vector,n,C,g,neighbors_list,neighbors_weights)
	
	f = zeros(Complex,n)
	next = zeros(Int,n)
	state = zeros(Int,n)
	local_holonomy = ones(Complex,n)
	holonomy_to_root = ones(Complex,n)
	total_holonomy = 1
	seen = zeros(Int,n) 
	degree = sum.(neighbors_weights)
	i = 1
	last_visited_node = 0

	roots = zeros(Int,n)
	root = 0
	root_value = zeros(Complex,n)
	root_size = zeros(Int,n)

	is_cycle = false
	
	for v in 1:n
		u = v
		is_unicycle = false
		last_visited_node = u
		total_holonomy = 1
		
		while state[u] != -1 && state != -2 
			seen[u] = i
			is_cycle = false
			deg = degree[u]
			if rand() <= q_vector[u]/(q_vector[u]+deg)
				state[u] = -1
				next[u] = u
				last_visited_node = u	

				root = u
				roots[u] = root
				root_size[u] += 1
				root_value[u] = g[u]
			else
				next[u] = sample(neighbors_list[u],Weights(neighbors_weights[u]))
				
				local_holonomy[next[u]] = local_holonomy[u]/C[u,next[u]]
				total_holonomy *= C[u,next[u]]
				
				is_cycle = detect_cycle(u,next[u],next,seen,i)

				u = next[u]

				value = f[u] 
				root = roots[u]
				last_visited_node = u
			end

			if is_cycle
				if rand() <= 1-cos(angle(local_holonomy[u]/(local_holonomy[next[u]]*C[u,next[u]]))) 
					state[u] = -2 
				else 
					local_holonomy[u] = local_holonomy[next[u]]*C[u,next[u]]
					total_holonomy = 1/local_holonomy[u]
				end
			end
		end

		i += 1

		# adding the new branch and accumulating the values at the root
		
		u = v
		total_holonomy *= holonomy_to_root[last_visited_node]

		while state[u] != -1 && (state[u] != -2 || state[next[u]] != -2)
			if state[last_visited_node] != -2
				root_value[root] += total_holonomy*g[u]
				roots[u] = root
				root_size[root] += 1
				state[u] = -1
			else
				state[u] = -2
			end

			holonomy_to_root[u] = total_holonomy
			total_holonomy /= C[u,next[u]]
			u = next[u]
		end
	end

	# computing f

	for v in 1:n
		if roots[v] != 0 && root_size[roots[v]] != 0
			f[v] = (holonomy_to_root[v]*root_value[roots[v]])/(root_size[roots[v]])
		end
	end

	return f
end

function rao_estimate(q_vector,n,C,g,nb_samples,neighbors_list,neighbors_weights)
	x = zeros(n)
	for k in 1:nb_samples
		x += rao_estimator(q_vector,n,C,g,neighbors_list,neighbors_weights)
	end
	x /= nb_samples
	return x
end

function var_reduced_rao_estimate(q,q_vector,n,C,g,nb_samples,D,neighbors_list,neighbors_weights,L)
	x = zeros(n)
	dmax = maximum(D)
	Q = Diagonal(diagm(q_vector))
	alpha = (2*q)/(q + 2*dmax)
	for k in 1:nb_samples
		x += rao_estimator(q_vector,n,C,g,neighbors_list,neighbors_weights)
	end
	x /= nb_samples
	return x - alpha*((inv(Q)*(L + Q)*x - g))
end
